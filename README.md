# DeepVox
DeepVox is a voxel engine I'm developping on my free time. I got no ambitions other than learning with this. If it works and can be use in project I'll be really glad (and surprise), but I do not expect it.
## Goal
* Understand how voxel works
* Continue to learn opengl/vulkan
* Get better at rust

## Non goal
* Make the engine of the year
* Save Kittens
* Make it "industrially ready"